<?xml version="1.0" encoding="utf-8"?>

<!--
Copyright (c) 2018,2019 Hocuri

This file is part of SuperFreezZ.

SuperFreezZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SuperFreezZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SuperFreezZ.  If not, see <http://www.gnu.org/licenses/>.
-->

<resources xmlns:tools="http://schemas.android.com/tools"  tools:ignore="MissingTranslation">

	<string name="app_name">SuperFreezZ</string>
	<string name="msg_search">Search</string>
	<string name="msg_search_help">Search&#8230;</string>
	<string name="share_exception">An exception occurred. Share infos using&#8230;</string>
	<string name="accessibility_service_description">Enabling this makes it possible to automate the freezing of
		apps.\n\nThis has the following advantages:\n - The user does not have to do this himself: It is way faster and
		the user does not have to care about how to do it.\n - Users with physical disabilities just need to start the
		shortcut "autofreeze" (by speech recognition, for example). Then they can freeze all apps automatically.
		\n\nSuperFreezZ just uses the accessibility service to tap on "Force Stop", "OK", "Back" and to evaluate the
		result of these interactions.
	</string>
	<string name="freeze_shortcut_label">Freeze</string>
	<string name="NothingToFreeze">Nothing to freeze</string>
	<string name="undo">Undo</string>
	<string name="icon">Icon</string>
	<string name="freeze_this_app_if_it_has_not_been_used_for_a_longer_time">Freeze this app if it has not been used for
		a longer time.
	</string>
	<string name="always_freeze_this_app">Always freeze this app</string>
	<string name="do_never_freeze_this_app">Do never freeze this app</string>
	<string name="pending_freeze">Pending Freeze</string>
	<string name="frozen">Frozen</string>
	<string name="freeze_off">Freeze off</string>
	<string name="used_recently">Used recently</string>
	<string name="space_AND_space">\u0020<![CDATA[&]]>\u0020</string>
	<string name="no_apps_pending_freeze">[NO APPS PENDING FREEZE]</string>
	<string name="all_apps">ALL APPS</string>
	<string name="usagestats_access">UsageStats access</string>
	<string name="usatestats_explanation">UsageStats access makes it possible for SuperFreezZ to freeze apps only if you
		did not use them for some time (AUTO FREEZE).
	</string>
	<string name="enable">Enable</string>
	<string name="usagestats_not_enabled">You did not enable usagestats access.</string>
	<string name="automate_the_freezing_of_apps">Automate the freezing of apps?</string>
	<string name="accessibility_arguments_yes">For users who: \n\u2022 want to have it as easy as possible \n\u2022 want
		to freeze a lot of apps
	</string>
	<string name="accessibility_arguments_no">You will have to press "FORCE STOP", "OK" and Back manually to freeze an
		app.\nFor users who: \n\u2022 have a slow phone \n\u2022 care a lot about security \n\u2022 do not want to
		register SuperFreezZ as an accessibility service
	</string>
	<string name="yes">Yes</string>
	<string name="no">No</string>
	<string name="welcome">Welcome to SuperFreezZ!</string>
	<string name="short_description">SuperFreezZ makes it easy to entirely freeze all background activities of apps.
	</string>
	<string name="intro_always_freeze_title">Freeze Mode: ALWAYS FREEZE</string>
	<string name="intro_always_freeze_explanation">There are 3 freeze modes. You can choose one for every app. ALWAYS
		FREEZE will freeze an app in any case.
	</string>
	<string name="intro_never_freeze_title">Freeze Mode: NEVER FREEZE</string>
	<string name="intro_never_freeze_explanation">NEVER FREEZE will - turn off freezing and never freeze an app.
	</string>
	<string name="intro_auto_freeze_title">Freeze Mode: AUTO FREEZE</string>
	<string name="intro_auto_freeze_explanation">AUTO FREEZE will freeze an app only if it has not been used for some
		time (7 days by default)
	</string>
	<string name="freeze_manually">Freeze manually</string>
	<string name="Press_forcestop_ok_back">To freeze an app, please press "FORCE STOP", then "OK", then Back</string>
	<string name="freeze_manually_no">I do not want to have to do this</string>
	<string name="title_activity_settings">Settings</string>

	<!-- Strings related to Settings -->

	<string name="pref_header_appslist">Apps list</string>


	<string-array name="appslist_show_special_titles">
		<item>Launcher</item>
		<item>SuperFreezZ itself</item>
		<item>System apps (the system will quickly defrost them)</item>
	</string-array>
	<string-array name="appslist_show_special_values"  translatable="false">
		<item>Launcher</item>
		<item>SuperFreezZ</item>
		<item>Systemapps</item>
	</string-array>


	<string name="pref_title_standard_freeze_mode">Standard freeze mode</string>
	<string-array name="standard_freeze_mode_titles">
		<item>Always freeze</item>
		<item>Never freeze</item>
		<item>Auto freeze</item>
	</string-array>
	<string-array name="standard_freeze_mode_values" translatable="false">
		<item>0</item>
		<item>1</item>
		<item>2</item>
	</string-array>

	<!-- Example settings for Data & Sync -->
	<string name="pref_header_about">About</string>

	<!-- Example settings for Notifications -->
	<string name="pref_header_freezing">Freezing apps</string>

	<string name="pref_ringtone_silent">Silent</string>

	<string name="appslist_show_special">Show special apps in the list</string>
	<string name="pref_autofreeze_delay">Freeze mode AUTO: Apps are frozen after… (in days)</string>
	<string name="pref_summary_appslist_show_special">The Launcher, SuperFreezZ itself and system apps are hidden by default.</string>
	<string name="create_freeze_shortcut">Create Freeze shortcut</string>
	<string name="settings">Settings</string>
	<string name="sort">Sort</string>
	<string name="pref_header_freezing_summary">Customize how and which apps are frozen</string>
	<string name="pref_header_about_summary">Useful links, send logs</string>
	<string name="pref_header_appslist_summary">Show special apps, standard freeze mode</string>
	<string name="freeze_apps_now">Freeze apps now</string>

	<string-array name="sortmodes">
		<item>Name</item>
		<item>Freeze state</item>
		<item>Last time used</item>
	</string-array>
</resources>
