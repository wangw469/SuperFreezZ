SuperFreezZ
===========

An android app that makes it possible to entirely freeze all background activities of an app.
Please note that this is a very early alpha version.

Greenify is another app that can do this, but it is not Open Source.

Any contributions are welcome.

SuperFreezZ is not yet another task manager promising to delete 10GB of data per month or making your device 2x as fast. This is impossible. You should freeze only
* apps that you do not trust (and do not want to run in background) and 
* apps that you use very few.


If you freeze apps that you use daily, the battery of your device will drain faster and these apps will take longer to load. You should take the name seriously: SuperFreezZ will super freeze your apps, and it is not that easy to defrost them.

Features
--------

* Optionally works without accessibility service as this slows down the device
* Option to freeze only apps that you did not use for a week

Build
-----

The build should succeed out of the box with Android Studio and Gradle. If not, it is probably my fault, please open an issue then. Others will probably also have this problem then.

Download
--------

[<img src="https://f-droid.org/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="70">](https://f-droid.org/packages/superfreeze.tool.android/)

Contributing to SuperFreezZ
------------

If you have a problem or a question or an idea or whatever, just open an issue!

If you would like to help, have a look at the issues or think about what could be improved and open an issue for it. Please tell me what you are going to do to avoid that I also implement the same thing at the same time :-)

You can also [donate](https://gitlab.com/SuperFreezZ/SuperFreezZ/issues/18) or [translate SuperFreezZ](https://gitlab.com/SuperFreezZ/SuperFreezZ/issues/21).

Donate
------

I do not accept monetary donations currently (I tried to set up a liberapay account but it was too much of an effort so I gave it up for the time being). 

However, to show me your support, you can [donate to WWF or the Christian Blind Mission and post about it here](https://gitlab.com/SuperFreezZ/SuperFreezZ/issues/18). I would maybe also have put Fairphone to the list as I really like the idea but there seems not to be an option to donate to them, either. So just let me point out here that Fairphone is selling an ethical and modular smartphone.

Credits
-------

I took the code to show the apps list from [apkExtractor](https://f-droid.org/wiki/page/axp.tool.apkextractor).

Copying
-------


```
Copyright (c) 2015 axxapy
Copyright (c) 2018 Hocuri

SuperFreezZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SuperFreezZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SuperFreezZ.  If not, see <http://www.gnu.org/licenses/>.
```
